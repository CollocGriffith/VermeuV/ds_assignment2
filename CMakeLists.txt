# CMake version and project name
cmake_minimum_required(VERSION 3.12)
project(ds_assignment2)

# CMake and Make configuration
set(CMAKE_CXX_STANDARD 17)

# Add project executable
add_executable(ds_assignment2 sources/main.cpp)

# Find and link OpenMPI
find_package(MPI REQUIRED)
target_include_directories(ds_assignment2 PRIVATE ${MPI_CXX_INCLUDE_PATH})
target_compile_options(ds_assignment2 PRIVATE ${MPI_CXX_COMPILE_FLAGS} ${MPI_CXX_LINK_FLAGS})
target_link_libraries(ds_assignment2 ${MPI_CXX_LIBRARIES})
