// simple MPI example that will show how to initialise, get a size and rank and also finalise
// MPI, these are steps that are an important part of all MPI programs

#include <iostream>
#include <mpi.h>
#include <cmath>

/*
 * Name: Valerian Vermeulen
 * Student Number: 2982269
 * CPU: Intel Core i7-5600U @ 4x 3.2GHz
 * Output:
 * - 3.6265e-05
 * - 0.00149368
 * - 0.113854
 * - 6.32552
 */


// primeCounter() accepts an integer n and returns an integer containing the amount of prime numbers in n.
int primeCounter(int n) {

  int count=0;

  for (int a=2 ; a < n ; a++) {

    bool isPrime = true;

    for (int c=2 ; c*c <= a ; c++) {

      if(a % c == 0) {

        isPrime = false;

        break;

      }

    }

    if(isPrime) count++;

  }

  return count;

}

int main(int argc, char **argv) {

  // initialise MPI this is the first thing that must happen at the start of any mpi program
  MPI::Init(argc, argv);
  
  // figure out what our rank is in the world
  int rank;
  rank = MPI::COMM_WORLD.Get_rank();

  // create an array which gathers results from nodes
  double gathers[4];
  // get the time before compute
  auto startTime = MPI::Wtime();
  // compute
  primeCounter(pow(25, rank + 2));
  // get the time after compute
  auto endTime = MPI::Wtime();

  // compute the time of the computing
  auto time = endTime - startTime;

  // gathers all results from the nodes in rank 0
  MPI::COMM_WORLD.Gather(&time, 1, MPI::DOUBLE, gathers, 1, MPI::DOUBLE, 0);

  // if we are in the node 0
  if (rank == 0) {
    // symbolize the node number
    auto root = 0;
    // for all items in gathers
    for (auto &gather : gathers) {
      // print the number of the node and how many time it take to compute the result
      std::cout << "Rank " << root++ << " took " << gather << " seconds to compute the result." << std::endl;
    }
  }

  // properly terminate the MPI
  MPI::Finalize();

  // all is good at this point so return true
  return 0;
}